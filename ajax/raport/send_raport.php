<?php
require_once '../../model/model.php';
require_once '../../core/helper.php';
require("../../core/phpmailer.php");
require("../../core/smtp.php");

date_default_timezone_set('Europe/Bucharest');

$date = date('Y-m-d H:i:s');
$text = $_GET['text'];

$text = nl2br($text);
if($_GET['send'] !== 'false')
{
	if(!!$_GET['to'] && strpos($_GET['to'], '@') !== false)
	{
		@$emails = explode(';', $_GET['to']);
		
		foreach($emails as $email)
		{
			$mail = new PHPMailer();

			$mail->IsSMTP();
			$mail->SMTPAuth = true;
			$mail->SMTPSecure = "ssl"; 
			$mail->Host = "plus.smtp.mail.yahoo.com";
			$mail->Port = 465; 
			$mail->Username = 'licenta.raport@yahoo.com';
			$mail->Password = "12345qwert"; 
			$mail->From = 'licenta.raport@yahoo.com';
			$mail->FromName = $_GET['user'];
			$mail->AddAddress($email);
			$mail->Subject = "Raport ". $date;
			$mail->Body = $text;

			if(!$mail->Send())
			{
				Helper::message('Mesajul nu s-a trimis <br>Mailer error: '. $mail->ErrorInfo, 'danger');
			}
			else
			{
				Helper::message('Raportul a fos trimis cu succes pe adresa '. $email, 'success');
			}
		}	
	}
	elseif(strpos($_GET['to'], '@') === false) {
		Helper::message('Nu a-ti introdus un email valid!!!', 'danger');
	}	
}


$conectInfo = array(
           'host' => 'localhost',
           'database' => 'firma_database',
           'username' => 'root',
           'password' => '',
           );

$m = new model($conectInfo);

if(!!$_GET['text']){
	$m->saveRaport($text, $date, $_GET['userId']);
	Helper::message('Raportul a fost salvat cu succes!', 'success');
}
else{
	Helper::message('Raportul este gol!!', 'danger');
}
