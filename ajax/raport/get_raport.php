<?php

require_once '../../model/model.php';
require_once '../../core/helper.php';
$conectInfo = array(
           'host' => 'localhost',
           'database' => 'firma_database',
           'username' => 'root',
           'password' => '',
           );

$m = new model($conectInfo);

if($_GET['data'])
{
	$date = DateTime::createFromFormat('d/M/Y', $_GET['data']);
	$date = $date->format('Y-m-d');

	$raport = $m->getRaport($date, $_GET['user']);

	if(!empty($raport)) {
		Helper::printRaport($raport[0]);
	}
	else {
		Helper::message('&#206;n data selectat&#259; nu s-a trimis raport', 'danger');
	}
}
else{
	Helper::message('Selecta&#355;i o dat&#259;!!!!', 'danger');
}