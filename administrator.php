<!DOCTYPE html>
<head>

<title>Licenta</title>
           
  <?php
    include "controller/controller.php";
    $controller = new Controller('administrator');
  ?>

<script type="text/javascript">
  $(document).ready(function() {
    $('#users_table').DataTable();
} );
</script>

</head>

<body>
  <?php
      $controller->getMeniu();
      $selector = $controller->model->getDistinctSelector();
      $department = $controller->getPostValue('select_department');
      $def = $controller->getPostValue('def');
      $functie = $controller->getPostValue('select_functie');
      $acces_index = $controller->getPostValue('select_acces_index');
  ?>
  <div class="container">
    <div class="row"> 
      <div class="col-md-2">
        <div class="row"> 
          <ul class="nav">
            <li class="active"><a href="#tab_a" data-toggle="tab">Administrare utilizatori</a></li>
            <!-- <li><a href="#tab_b" data-toggle="tab">Electric Admin</a></li>
            <li><a href="#tab_c" data-toggle="tab">Other Admin</a></li>
 -->            <li><a href="#tab_d" data-toggle="tab">Instalere unit&#259;&#355;i</a></li>
          </ul>
        </div>
      </div>
    
      <div class="col-md-10">
        <div class="tab-content">
          <div class="tab-pane active" id="tab_a">
            
              <form method="POST" action="administrator.php">
                <input hidden name="def"  id="def" value="1"/>
                <div class="row">
                  <div class="col-md-4">
                    <span>Departament</span>
                  
                    <select name="select_department" class="form-control">
                      <option >oricare</option>
                      <?php
                        foreach($selector['department'] as $key => $dep)
                        {
                          echo '<option value="', $key,'" ', $key == $department ? 'selected' : '','>', $key,'</option>';
                        }
                      ?>
                    </select>
                  </div>
                  <div class="col-md-3">
                    <span>Func&#355;ie</span>
                  
                    <select name="select_functie" class="form-control">
                        <option>oricare</option>
                        <?php
                          foreach($selector['functie'] as $key => $dep)
                          {
                            echo '<option value="', $key,'" ', $key == $functie ? 'selected' : '','>', $key,'</option>';
                          }
                        ?>
                      </select>
                  </div>
                  <div class="col-md-3">
                    <span>Acces</span>
                  
                    <select name="select_acces_index" class="form-control">
                        <option >oricare</option>
                        <option value="0">Utilizatori noi</option>
                        <?php
                        $acces_array = array(
                                              'Utilizatori noi',
                                              'Administrator',
                                              '&#350;ef de echip&#259;',
                                              'Utilizator simplu',    
                                             );
                          foreach($selector['acces_index'] as $key => $dep)
                          {
                            echo '<option value="', $key,'" ', $key == $acces_index ? 'selected' : '','>', $acces_array[$key],'</option>';
                          }
                        ?>
                      </select>
                  </div>

                  <div class="col-md-1">
                    <input type="submit" value="Filtrare" id="sort_users" class="btn btn-default"/>
                  </div>
                </div>
              </form>
              <hr>
              <div id="load_container">
                
              </div>
              
              <table id="users_table" class="table table-striped"  cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <td>Id utilizator</td>
                    <td>Nume</td>
                    <td>Nume utilizator</td>
                    <td>Departament</td>
                    <td>Acces</td>
                    <td>Func&#355;ie</td>
                    <td>Editare</td>
                  </tr>
                </thead>
                  <?php

                      $users = $controller->model->getAllUsers($department, $functie, $acces_index, $def);
                      include('view/admin/print_all_users.php');
                  ?>
                
              </table>
              
            
          </div>

          <div class="tab-pane" id="tab_b">
            <div class="table-responsive">
            </div>
          </div>

          <div class="tab-pane" id="tab_c">
            <div class="table-responsive">

            </div>
          </div>
          <div class="tab-pane" id="tab_d">
            <div class="table-responsive">
            <?php include('view/admin/item_install_view.php'); ?>
            </div>
          </div>
                
        </div>
      </div>
    </div>
  </div>
</body>
