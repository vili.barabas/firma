<?php


if(!empty($users))
	foreach($users as $user)
	{
		echo '
			<tr>
				<td>', $user->user_id,'</td>
				<td>', $user->name,'</td>
				<td>', $user->username,'</td>
				<td>', $user->department,'</td>
				<td>', $acces_array[$user->acces_index],'</td>
				<td>', $user->functie,'</td>
				<td><button type="button" class="btn btn-info edit_user" data-toggle="modal" data-target="#userModel_', $user->user_id,'" id="userModel_', $user->user_id,'">Editare</button>
					<a target="_blank" class="btn btn-info edit_user" href="http://localhost/gitlab/firma/profil.php?id=', $user->user_id,'&profil">Profil <span class="glyphicon glyphicon-share-alt"></span></a>
				</td>
			</tr>
		';
		echo '<div id="userModel_', $user->user_id,'" class="modal fade" role="dialog">
                  <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Editare Utilizator</h4>
                      </div>
                      <div class="modal-body">
                        <div class="row">
							<div class="col-md-4">
								<strong> Id Utilizator: </strong>
							</div>
							<div class="col-md-8">
								<input disabled class="form-control" id="edit_user_id" value="', $user->user_id,'"/>
							</div>                        
                        </div>
                        <div class="row">
							<div class="col-md-4">
								<strong> Nume: </strong>
							</div>
							<div class="col-md-8">
								<input class="form-control" id="edit_user_name" value="', $user->name,'"/>
							</div>                        
                        </div>
                        <div class="row">
							<div class="col-md-4">
								<strong> Nume Utilizator: </strong>
							</div>
							<div class="col-md-8">
								<input class="form-control" id="edit_userName" value="', $user->username,'"/>
							</div>                        
                        </div>
                        <div class="row">
							<div class="col-md-4">
								<strong> Departament: </strong>
							</div>
							<div class="col-md-8">
								
								<select id="edit_user_department" class="form-control">
		                        <option></option>
		                        ';
		                          foreach($selector['department'] as $key => $dep)
		                          {
		                            echo '<option value="', $key,'" ', $key == $user->department ? 'selected' : '','>', $key,'</option>';
		                          }
		                  echo '      
		                      </select>
							</div>                        
                        </div>
                        <div class="row">
							<div class="col-md-4">
								<strong> Acces: </strong>
							</div>
							<div class="col-md-8">
								
								<select id="edit_user_acces_index" class="form-control">
		                        <option value="0">Utilizator nou</option>
		                        ';

		                        $acces_array = array(
		                                              'Utilizator nou',
		                                              'Administrator',
		                                              '&#350;ef de echip&#259;',
		                                              'Utilizator simplu',    
		                                             );
		                          foreach($selector['acces_index'] as $key => $dep)
		                          {
		                            echo '<option value="', $key,'" ', $key == $user->acces_index ? 'selected' : '','>', $acces_array[$key],'</option>';
		                          }
		                  echo '      
		                      </select>

							</div>                        
                        </div>
                        <div class="row">
							<div class="col-md-4">
								<strong> Func&#355;ie: </strong>
							</div>
							<div class="col-md-8">
								
							
								<select id="edit_user_functie" class="form-control">
		                        <option></option>
		                        ';
		                          foreach($selector['functie'] as $key => $dep)
		                          {
		                            echo '<option value="', $key,'" ', $key == $user->department ? 'selected' : '','>', $key,'</option>';
		                          }
		                  echo '      
		                      </select>
							</div>                        
                        </div>
                      </div>
                      <div class="modal-footer">
                      <div class="row">
							<div class="col-md-2">
								<div class="save_edit_user">
									<button type="button" class="btn btn-info float-left"  data-dismiss="modal" id="user_', $user->user_id,'">Salveaz&#259;</button>
								</div>
							</div>
							<div class="col-md-10">
								<button type="button" class="btn btn-default" data-dismiss="modal">Renun&#355;&#259;</button>
							</div>                        
                        </div>
                        
                      </div>
                    </div>

                  </div>
                </div>';
	}