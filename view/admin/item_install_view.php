<div id="item_accept_message">
</div>
<div class="table">
<table class="table">
	<tr>
		<th>Nr. &#206;nregistrare</th>
		<th>Nume</th>
		<th>Departament</th>
		<th>Unitate</th>
		<th>&#350;ef echip&#259;</th>
		<th>Cantitate</th>
		<th></th>
	</tr>

<?php

	$item_queryes = $controller->model->getItemQuery($_SESSION['UserData']->department, $_SESSION['UserData']->acces_index);
	$row = array(2 => 'danger', 0 => 'warning', 1 => 'succs');
	$accept_leader = array(2 => 'refuzat', 0 => 'neprocesat', 1 => 'acceptat');
	if(!empty($item_queryes))
	{
		foreach($item_queryes as $itm)
		{
			echo "<tr class='". $row[$itm->team_leader_accept]."'>
				<td class='register_nr'>". $itm->nr."</td>
				<td class='user_name'>". $itm->userName."</td>
				<td class='department'>". $itm->department."</td>
				<td class='item_name'>". $itm->itemName."</td>
				<td >". $accept_leader[$itm->team_leader_accept]."</td>
				<td>1</td>
				<td><span class='done_button'>
				<button class='btn btn-primary'", $itm->team_leader_accept !== 1 ? " disabled='disabled'" : "", "'><span class='glyphicon glyphicon-ok'></span> Instalat</button></span> 
				<span class='delete_button'><button class='btn btn-danger'><span class='glyphicon glyphicon-remove'></span> &#350;terge cerere</button></span></td>
			</tr>";
		}
	}
	else
	{
		echo '<p>Momentan nu exista nici o cerere de unitati!</p>';
	}
?>

</table>
</div>