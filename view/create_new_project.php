<div class="modal fade" id="myModalCreate" role="dialog">
	<div class="modal-dialog">
  <!-- Modal content-->
	  	<div class="modal-content">
		    <div class="modal-header">
		      <button type="button" class="close" data-dismiss="modal">&times;</button>
		      <h4 class="modal-title">Creearea unui nou proiect</h4>
		    </div>
		    <div class="modal-body">
				<div class="row">
		 			<div class="col-md-3">
						<span>Titlu:</span>
					</div>
					<div class="col-md-5">
						<input id="create_task_name" value="">
					</div>
				</div>
				<div class="row">
		 			<div class="col-md-3">
						<span>Time estimat de lucru:</span>
					</div>
					<div class="col-md-4">
						<input id="create_task_time" value="">
					</div>
					<div class="col-md-3">
						<span>ore</span>
					</div>
				</div>
				<div class="row">
		 			<div class="col-md-3">
		 				<span>Descriere:</span>
					</div>
					<div class="col-md-5">
						<textarea rows="4" cols="50" id="create_task_description"></textarea>
					</div>
				</div>
				<div class="row">
		 			<div class="col-md-3">
		 				<span>Observa&#355;ii:</span>
					</div>
					<div class="col-md-5">
						<textarea rows="4" cols="50"  id="create_task_observation"></textarea>
					</div>
				</div>
	            <div class="row">
		 			<div class="col-md-3">
					</div>
	            	<div class="col-md-5">
						<button id="create_project" type="button" class="btn btn-success" data-dismiss="modal">Creeare</button>
					</div>
	            </div>
			</div>
	  	</div>
	</div>
</div>
